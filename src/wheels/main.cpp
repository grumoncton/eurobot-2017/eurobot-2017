#include "Arduino.h"
// #include "RoboClaw2.h"
#include "RoboClaw.h"

// RoboClaw2 roboclaw(15, 14, 3);
RoboClaw roboclaw(&Serial3, 10000);

#define ADDRESS 0x80

uint8_t flag = 1;
uint32_t speed = 2000;
uint32_t distance = 1000;

void setup() {
  roboclaw.begin(2400);
  roboclaw.ResetEncoders(ADDRESS);
}

void loop() {
  roboclaw.SpeedDistanceM1M2(ADDRESS, speed, distance, speed, distance, flag);
  delay(3000);
  roboclaw.SpeedDistanceM1M2(ADDRESS, -speed, distance, -speed, distance, flag);
  delay(3000);
}
