//Roboclaw simple serial example.  Set mode to 6.  Option to 4(38400 bps)
#include <SoftwareSerial.h>

 int val; 
 int encoder0PinA = 4;
 int encoder0PinB = 5;
 float encoder0Pos = 0;
 int encoder0PinALast = LOW;
 int n = LOW;

 float encoder_pulse = 400;
 float wheel_perimeter = 200;
 float pulse_distance = wheel_perimeter / encoder_pulse;
 
void setup() {
  pinMode (encoder0PinA,INPUT);
  pinMode (encoder0PinB,INPUT);
  Serial.begin(9600);
}


void loop() {

    // int inByte = Serial.read();

   n = digitalRead(encoder0PinA);
   if ((encoder0PinALast == LOW) && (n == HIGH)) {
     if (digitalRead(encoder0PinB) == LOW) {
       encoder0Pos = encoder0Pos - pulse_distance;
     } else {
       encoder0Pos = encoder0Pos + pulse_distance;
     }
     Serial.println(encoder0Pos);
   } 
   encoder0PinALast = n;
  
//    Encodeur();
//    switch (inByte) {
//      case '1': // Mets en marche les moteurs pour deplacement
//        for (int i=64; i < 127; i++){
//          Encodeur();
//          Serial.write(i);
//          Serial.write(i+127);
//          delay(50);
//        }
//        break;
//        
//      case '2': // Fin du deplacement
//        for (int i=127; i > 64; i--){
//          Encodeur();
//          Serial.write(i);
//          Serial.write(i+127);
//          delay(50);
//        }
//        break;
//      
//      case '3': // Mets en marche les moteurs pour rotation
//        for (int i=64; i < 80; i++){
//          Encodeur();
//          Serial.write(i);
//          Serial.write(256-i);
//          delay(50);
//        }
//        break;
//        
//      case '4': // Fin de la rotation
//        for (int i=80; i > 64; i--){
//          Encodeur();
//          Serial.write(i);
//          Serial.write(256-i);
//          delay(50);
//        }
//        break;
//    }
   
    
}


void Encodeur() {
    
    n = digitalRead(encoder0PinA);
//    Serial.print("EncoderA: ");
//    Serial.println(n);
   if ((encoder0PinALast == LOW) && (n == HIGH)) {
     if (digitalRead(encoder0PinB) == LOW) {
       encoder0Pos = encoder0Pos - pulse_distance;
     } else {
       encoder0Pos = encoder0Pos + pulse_distance;
     }
     Serial.println(encoder0Pos);
   } 
   encoder0PinALast = n;
  
}
