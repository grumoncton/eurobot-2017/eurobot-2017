#include "Arduino.h"
#include "Ultra.h"
#include "UltraSet.h"
;

Ultra sensors[3];
int dists[] { 0, 0, 0 };

UltraSet::UltraSet(Ultra const& l, Ultra const& c, Ultra const& r) {
  sensors[0] = l;
  sensors[1] = c;
  sensors[2] = r;
}

String UltraSet::distString() {
  const String space = String(" ");
  return String(dists[0] + space + dists[1] + space + dists[2]);
}

void UltraSet::read() {
  for (int i = 0; i < 3; i++) {
    dists[i] = sensors[i].read();
  }
}
