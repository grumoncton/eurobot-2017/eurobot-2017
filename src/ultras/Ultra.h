#ifndef Ultra_h
#define Ultra_h

#include "Arduino.h"
#include "main.h"

class Ultra {

public:

  int echo, trig;

  Ultra(int echo, int trig);
  Ultra();
  long read();

};

#endif
