#include "Arduino.h"
#include "main.h"
#include "Ultra.h"
#include "UltraSet.h"
#include "Direction.h"
;


//
// Set up ultrasounds
//
UltraSet front(Ultra(2, 3), Ultra(4, 5), Ultra(6, 7));
UltraSet back(Ultra(8, 9), Ultra(10, 11), Ultra(12, 13));

// Current direction
// Sent by drive arduino to select which ultras to use (font/back)
Direction direction = Forwards;

void setup() {
  Serial.begin(9600);
  pinMode(ALERT_PIN, OUTPUT);
}

void loop() {

  // If message in buffer, parse it
  if (Serial.available() > 0) {
    String message = Serial.readStringUntil('\n');

    switch (message[0]) {
      case 'f':
        direction = Forwards;
        break;
      case 'b':
        direction = Backwards;
        break;

      // Okay
      // Sent when other arduino reads the alert signal
      case 'o':
        digitalWrite(ALERT_PIN, LOW);
        break;
    }
  }

  // Get set we should use
  // front for Forwards
  // back for Backwards
  UltraSet current = direction == Forwards ? front : back;

  // Read each ultra
  current.read();

  for (int i = 0; i < 3; i++) {

    // If we're too close
    if (current.dists[i] < TOLERANCE) {

      // Print stop message
      Serial.println("s");

      // Trigger alert pin
      digitalWrite(ALERT_PIN, HIGH);
    }
  }

  #ifdef DEBUG
    Serial.println(current.distString());
  #endif

}
