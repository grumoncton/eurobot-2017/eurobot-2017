// Maximum value (mm) that triggers alert
#define TOLERANCE 300

// Pin that will be set high if we're too close
#define ALERT_PIN 14
