#include "Arduino.h"
#include "main.h"
#include "Ultra.h"
#include "units.h"
;

const int TIMEOUT = millimetersToMicroseconds(TOLERANCE) * 3 / 2;

int echo, trig;

Ultra::Ultra(int _echo, int _trig) {
  echo = _echo;
  trig = _trig;

  pinMode(echo, INPUT);
  pinMode(trig, OUTPUT);
}

Ultra::Ultra() { }

long Ultra::read() {
  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);

  long timeDiff = pulseIn(echo, HIGH, TIMEOUT);
  // Serial.println(timeDiff);

  if (timeDiff == 0) {
    return TOLERANCE;
  }

  long distance = microsecondsToMillimeter(timeDiff);

  return distance;
}
