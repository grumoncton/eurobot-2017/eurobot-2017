#ifndef units_h
#define units_h

long microsecondsToMillimeter(long microseconds) {
  return microseconds * 5 / 29;
}

long millimetersToMicroseconds(long millimeters) {
  return millimeters * 29 / 5;
}

#endif
