#ifndef UltraSet_h
#define UltraSet_h

#include "Arduino.h"

class UltraSet {

public:

  Ultra sensors[3];
  int dists[3];
  UltraSet(Ultra const& l, Ultra const& c, Ultra const& r);
  void read();
  String distString();

};

#endif
