#include "Arduino.h"
#include "RoboClaw.h"
#include "Robot.h"
#include "Grabber.h"
#include "Servo.h"
#include "Side.h"
#include "Bras.h"
#include "ObstacleDetection.h"
#include "Color.h"
#include "Cones.h"
;

#define DEBUG

#define CONE_CENTRE_TO_ROBOT_CENTRE 158.5f
#define OFFSET_RAIL_TO_ROBOT 30
#define HALF_RAIL_WIDTH 68
#define ROBOT_CENTRE_TO_ROBOT_EDGE 105.08f

RoboClaw roboclaw(&Serial3, 10000);
#define address 0x80

#define X_START 0
#define Y_START 0
#define A_START 90

double    xPos      =  X_START;
double    yPos      =  Y_START;
double    anglePos  =  A_START;
double    anglePosRad = anglePos * PI / 180;

double    xDest      =  0;
double    yDest      =  0;
double    Dest       =  sqrt(sq(xDest) + sq(yDest)) ;
double    angleDest  =  anglePos ;

double xTarget, yTarget, aTarget;
Direction targetDirection;

int32_t enc1             =  0;     // position left encoder
int32_t enc2             =  0;     // position right encoder
int32_t speed1           =  0;     // speed left encoder
int32_t speed2           =  0;     // speed right encoder

double Dr  =  0;
double Dl  =  0;
double D   =  0;

double previousRightCount;
double previousLeftCount;
double difference;

float speed_error     =  0;
float pos_error       =  0;
float last_pos_error  =  0;
float M2_error        =  0;

bool foward;
double contraint_speed  =  25;  // Motor speed
double normal_speed     =  25;
double rotation_speed   =  45;
double rotation_speed2   = 30;
double kp               =  1;
double ki               =  1;
double kd               =  15;
int tolerance        = 350;
int rotation_tolerance = 300;
int rotation_tolerance2 = 150;

double encoderSeparation    =  105;
double trackWidth           =  PI * encoderSeparation;                 // mm/rotation
double wheelPerimeter       =  200;                                    // mm/revolution
double countsPerRevolution  =  4096 ;                                   // counts/revolution
double CountsPerDistance    =  countsPerRevolution / wheelPerimeter ;  // counts/mm
double CountsPerDegree      =  CountsPerDistance * trackWidth / 360 ;  // counts/degree
bool shouldResume = false;

//
// Definitions
//

bool limitSwitchState = false;

#define SIDE_PIN 36
#define LIMIT_SWITCH_PIN 37
#define FUNNY_ACTION_PIN 43

HardwareSerial ultraSerial = Serial2;


//
// Robot state
//

Robot robot;

// Starting table side
// Get set to proper value in setup()
TableSide tableSide = Blue;

long startMillis = 0;


//
// Servos
//

// Arm servos
Servo rightCylServo;
Servo rightSpinServo;
Servo rightOpenCloseServo;

Servo leftCylServo;
Servo leftSpinServo;
Servo leftOpenCloseServo;

// Grabber servos
Servo grabberBaseServo;
Servo grabberRightServo;
Servo grabberLeftServo;

Servo funnyActionServo;

// Color sensors
Color color;


//
// Classes
//

// Cylinder classes
Grabber grabber(grabberBaseServo, grabberRightServo, grabberLeftServo);

Bras bras(rightCylServo, rightOpenCloseServo, rightSpinServo, leftCylServo, leftOpenCloseServo, leftSpinServo, 0, 0, color);

Cones cones(robot, grabber, bras);

// Communication with ultra Arduino
ObstacleDetection obstacleDetection(ultraSerial);


void performChecks();
void stop() {
  roboclaw.ForwardM1(address,0);
  roboclaw.ForwardM2(address,0);
  last_pos_error = 0;
  M2_error = 0;
  previousRightCount = 0;
  previousLeftCount = 0;
}

void displayspeed() {
  performChecks();

  uint8_t status1,status2,status3,status4;
  bool valid1,valid2,valid3,valid4;

  enc1 = roboclaw.ReadEncM1(address, &status1, &valid1);
  enc2 = roboclaw.ReadEncM2(address, &status2, &valid2);
  speed1 = roboclaw.ReadSpeedM1(address, &status3, &valid3);
  speed2 = roboclaw.ReadSpeedM2(address, &status4, &valid4);

  speed_error = speed1 - speed2;
  pos_error = enc1 - enc2;

  Serial.print("Encoder1:");
  Serial.print(enc1,DEC);
  Serial.print(" ");
  Serial.print(speed1,DEC);
  Serial.print("     ");

  Serial.print("Encoder2:");
  Serial.print(enc2,DEC);
  Serial.print(" ");
  Serial.print(speed2,DEC);
  Serial.print("      ");

  Serial.print("Error:");
  Serial.print(pos_error);
  Serial.print(" ");
  Serial.print(speed_error);
  Serial.print("      ");

  Serial.print(" x: ");
  Serial.print(xPos);
  Serial.print(" y: ");
  Serial.print(yPos);
  Serial.print(" a: ");
  Serial.print(anglePos);
  Serial.println();

  Dr = (enc2 - previousRightCount) / CountsPerDistance;
  previousRightCount = enc2;
  Dl = (enc1 - previousLeftCount) / CountsPerDistance;
  previousLeftCount = enc1;

  D = (Dr + Dl) / 2;
  xPos += D * cos(anglePosRad);
  yPos += D * sin(anglePosRad);

  difference = Dr - Dl;
  anglePosRad += asin(difference / encoderSeparation);
  anglePos = anglePosRad * 180 / PI;

  robot.xPos = xPos;
  robot.yPos = yPos;
  robot.aPos = anglePos;
}

void motor_move(double distance) {

  if (distance >= 0) {
    foward = 1;
  }
  else {
    foward = 0;
  }
  distance = abs(distance * CountsPerDistance);
  Serial.println(distance);

  roboclaw.ResetEncoders(address);
  delay(10);

  if (foward == 1) {
    roboclaw.ForwardM1(address,contraint_speed);
    roboclaw.ForwardM2(address,contraint_speed);
    while(1){
      displayspeed();
      M2_error += ((pos_error*kp) + ((pos_error-last_pos_error)*kd)) / 230;
      last_pos_error = pos_error;
      roboclaw.ForwardM2(address,contraint_speed + M2_error);
      if (enc1 >= distance-tolerance){
        roboclaw.ForwardM1(address,0);
        roboclaw.ForwardM2(address,0);
        delay(100);
        displayspeed();
        break;
      }
    }
  }

  else if (foward == 0) {
    roboclaw.BackwardM1(address,contraint_speed);
    roboclaw.BackwardM2(address,contraint_speed);
    while(1){
      displayspeed();
      M2_error += ((pos_error*kp) + ((pos_error-last_pos_error)*kd)) / 300;
      last_pos_error = pos_error;
      roboclaw.BackwardM2(address,contraint_speed - M2_error);
      if (enc1 <= -distance+tolerance){
        roboclaw.BackwardM1(address,0);
        roboclaw.BackwardM2(address,0);
        delay(100);
        displayspeed();
        break;
      }
    }
  }
  last_pos_error = 0;
  M2_error = 0;
  previousRightCount = 0;
  previousLeftCount = 0;
}

void motor_rotate(float angle) {

  double angle_distance = abs(angle * CountsPerDegree);
  Serial.println(angle_distance);

  roboclaw.ResetEncoders(address);
  delay(10);
  
  int speed = angle_distance > rotation_tolerance ? rotation_speed : rotation_speed2;

  if (angle >= 0) {
    roboclaw.BackwardM1(address,speed);
    roboclaw.ForwardM2(address,speed);
    while(1){
      displayspeed();
      pos_error = -enc1 - enc2;
      M2_error += ((pos_error*kp) + ((pos_error-last_pos_error)*kd)) / 1000;
      last_pos_error = pos_error;
      roboclaw.ForwardM2(address,speed + M2_error);
      if (angle_distance < rotation_tolerance){
        if (enc1 <= -angle_distance+rotation_tolerance2){
          roboclaw.ForwardM1(address,0);
          roboclaw.BackwardM2(address,0);
          delay(100);
          displayspeed();
          break;
        }
      }
      else {
        if (enc1 <= -angle_distance + rotation_tolerance){
          roboclaw.ForwardM1(address,0);
          roboclaw.BackwardM2(address,0);
          delay(100);
          displayspeed();
          break;
        }
      }
    }
  }

  else if (angle < 0) {
    roboclaw.ForwardM1(address,speed);
    roboclaw.BackwardM2(address,speed);
   while(1){
     displayspeed();
      pos_error = -enc1 - enc2;
      M2_error += ((pos_error*kp) + ((pos_error-last_pos_error)*kd)) / 1000;
      last_pos_error = pos_error;
      roboclaw.BackwardM2(address,speed - M2_error);
      if (angle_distance < rotation_tolerance){
        if (enc1 >= angle_distance-rotation_tolerance2){
          roboclaw.ForwardM1(address,0);
          roboclaw.BackwardM2(address,0);
          delay(100);
          displayspeed();
          break;
        }
      }
      else {
        if (enc1 >= angle_distance-rotation_tolerance){
          roboclaw.ForwardM1(address,0);
          roboclaw.BackwardM2(address,0);
          delay(100);
          displayspeed();
          break;
        }
      }
    }
  }
  last_pos_error = 0;
  M2_error = 0;
  previousRightCount = 0;
  previousLeftCount = 0;
}

void moveTo(double x, double y, Direction direction)  {
//  if (side) {
//    y = -y;
//  }

  xDest = x - xPos;
  yDest = y - yPos;
  Dest  = sqrt(sq(xDest) + sq(yDest)) ;
  angleDest = (atan2 (yDest,xDest )) * 180 / PI;
  double diff_angleDest = angleDest - anglePos;

  switch (direction) {
  case Forwards:
    ultraSerial.write('f');
    break;
  case Backwards:
    ultraSerial.write('b');

    // Switch things based on side
    angleDest += 180;
    diff_angleDest = diff_angleDest > 180 ? diff_angleDest - 180 : diff_angleDest + 180;
    Dest = -Dest;
  }

  if (diff_angleDest > 180) {
    diff_angleDest -= 360 ;
  }
  else if (diff_angleDest < -180) {
    diff_angleDest += 360 ;
  }

  Serial.print(angleDest);
  Serial.print("     ");
  Serial.print(diff_angleDest);
  Serial.print("     ");
  Serial.println(Dest);

  while (diff_angleDest >= 1 || diff_angleDest <= -1) {
    motor_rotate(diff_angleDest);

    diff_angleDest = angleDest - anglePos;
    if(direction == 0){
      angleDest = angleDest+180;
      diff_angleDest = diff_angleDest > 180 ? diff_angleDest - 180 : diff_angleDest + 180;
      Dest = -Dest;
    }

    if (diff_angleDest > 180) {
      diff_angleDest -= 360 ;
    }
    else if (diff_angleDest < -180) {
      diff_angleDest += 360 ;
    }
  }

  delay(100);
  motor_move(Dest);

//  xPos = x;
//  yPos = y;
//  anglePos = angleDest;
}

void rotate(double angle) {

  angleDest = angle;
  double diff_angleDest = angleDest - anglePos;

  while (diff_angleDest > 180) {
    diff_angleDest -= 360 ;
  }
  while (diff_angleDest < -180) {
    diff_angleDest += 360 ;
  }

  Serial.print(angleDest);
  Serial.print("     ");
  Serial.println(diff_angleDest);

  while (diff_angleDest >= 1 || diff_angleDest <= -1) {
    motor_rotate(diff_angleDest);

    diff_angleDest = angleDest - anglePos;

    if (diff_angleDest > 180) {
      diff_angleDest -= 360 ;
    }
    else if (diff_angleDest < -180) {
      diff_angleDest += 360 ;
    }
  }
}

void moveRelative(double x, double y, Direction direction) {

  // Calculate differences
  double diffX = x - xPos;
  double diffY = y - yPos;

  // Calculate target angle
  double targetA = atan(diffY / diffX);
  if (diffY < 0) targetA = - targetA;

  // How far away are we?
  double dist = sqrt(sq(diffX) + sq(diffY));

  // Calculate target coords
  double targetX = xPos + dist * cos(targetA);
  double targetY = yPos + dist * sin(targetA);

  moveTo(targetX, targetY, direction);
}


void resume() {
  if (!shouldResume) return;

  moveTo(xTarget, yTarget, targetDirection);
  rotate(aTarget);
}

//
// Check function
// Function passed from main to perform all necessary checks while stuck in while loop
//

void performChecks() {


  //
  // Ultras
  //

  if (obstacleDetection.shouldStop()) {

    // Stop motors
    stop();
    shouldResume = true;

    // Give Ultras enough time to send again
    delay(500);

    // Try again
    return;

  } else {

    // Otherwise, try to resume
    resume();
    shouldResume = false;
  }


  //
  // Timeout
  //

  if (millis() - startMillis > 90000) {
    stop();
    delay(1000);

    // Do funny action
    funnyActionServo.attach(FUNNY_ACTION_PIN);
    funnyActionServo.write(90);

    while(true);
  }
}


//
// Setup
//

void setup() {
  Serial.begin(9600);
  roboclaw.begin(38400);
  roboclaw.ResetEncoders(address);
  pinMode(LIMIT_SWITCH_PIN, INPUT);


  // 
  // Passthoughs
  //

  cones.moveTo = *moveTo;
  cones.moveRelative = moveRelative;
  cones.rotate = rotate;


  //
  // Setup color sensor
  //

  if(!color.tcs.begin()){
    while(true) {
      Serial.println("Can't find color sensor");
    }
  }; //color_Droite et gauche on les même pin
  color.tcs.write8(TCS34725_PERS, TCS34725_PERS_NONE);


  //
  // Attach servos
  //

  grabberBaseServo.attach(2);
  grabberRightServo.attach(3);
  grabberLeftServo.attach(29);

  rightCylServo.attach(5);
  rightCylServo.write(INIT_POS_CYL_RIGHT);
  rightOpenCloseServo.attach(4);
  rightOpenCloseServo.write(INIT_POS_OPEN_CLOSE_RIGHT);

  leftCylServo.attach(8);
  leftCylServo.write(INIT_POS_CYL_LEFT);
  leftOpenCloseServo.attach(9);
  leftOpenCloseServo.write(INIT_POS_OPEN_CLOSE_LEFT);


  //
  // Side pin
  //

  pinMode(SIDE_PIN, INPUT);
  if (digitalRead(SIDE_PIN)) {
    tableSide = Blue;
  } else {
    tableSide = Yellow;
  }

  cones.tableSide = tableSide;
  bras.tableSide = tableSide;

}


void loop() {


  //
  // Limit switch
  //

  if (!limitSwitchState) {

    // Get switch state
    limitSwitchState = digitalRead(LIMIT_SWITCH_PIN);

    // Return if it's still in
    return;
  }

  // Set startMillis if it's not set (should mean first loop)
  if (startMillis == 0) startMillis = millis();

  #ifdef DEBUG
  Serial.print("Game started at ");
  Serial.println(startMillis);
  #endif


  //
  // Main code
  //

//  rotate(0);
//  rotate(90);
  moveTo(0, 1000, Forwards);
  //moveTo(0, 0, Backwards);

  // robot.moveTo(0, 200, Forwards);
  // delay(2000);
  // robot.moveTo(0, 0, Backwards);

//   cones.getCone(0, 300, 2);
  while(1) {
    performChecks();
  }

}

