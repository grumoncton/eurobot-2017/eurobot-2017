#include "Arduino.h"
#include "RoboClaw.h"
#include "Servo.h"
#include "Side.h"
#include "Robot.h"
#include "Dumbo.h"
#include "ESC.h"
#include "TableSide.h"
#include "Direction.h"
#include "ObstacleDetection.h"
;

//
// Definitions
//

#define X_START 180
#define Y_START 130
#define A_START 90

bool limitSwitchState = false;

#define SIDE_PIN 13
#define LIMIT_SWITCH_PIN 12
#define address 0x80

HardwareSerial ultraSerial = Serial2;


//
// Robot state
//

// Starting table side
// Get set to proper value in setup()
TableSide tableSide = Blue;

long startMillis = 0;

bool shouldResume = false;


//
// Classes
//

// Robot class
Robot robot();

RoboClaw roboclaw(&Serial3, 10000);

Dumbo dumbo;
ESC Esc;


// Communication with ultra Arduino
ObstacleDetection obstacleDetection(ultraSerial);


double    xPos      =  X_START;
double    yPos      =  Y_START;
double    anglePos  =  A_START;

double xTarget, yTarget, aTarget;
Direction targetDirection;

double    xDest      =  0;
double    yDest      =  0;
double    Dest       =  sqrt(sq(xDest) + sq(yDest)) ;
double    angleDest  =  anglePos ;

int32_t enc1             =  0;     // position left encoder
int32_t enc2             =  0;     // position right encoder
int32_t speed1           =  0;     // speed left encoder
int32_t speed2           =  0;     // speed right encoder

double Dr  =  0;
double Dl  =  0;
double D   =  0;

double previousRightCount;
double previousLeftCount;
double difference;

float speed_error     =  0;
float pos_error       =  0;
float last_pos_error  =  0;
float M2_error        =  0;

bool foward;
double contraint_speed  =  20;  // Motor speed
double normal_speed     =  25;
double rotation_speed   =  22;
double ramp_speed       =  60;
double deramp_speed     =  32;
double kp               =  1;
double kd               =  15;
double tolerance        =  1000;
double rotation_tolerance =  700;
double rotation_tolerance2 = 150;

double encoderSeparation    =  120;
double trackWidth           =  PI * encoderSeparation;                 // mm/rotation
double wheelPerimeter       =  200;                                    // mm/revolution
double countsPerRevolution  =  10000 ;                                   // counts/revolution
double CountsPerDistance    =  countsPerRevolution / wheelPerimeter ;  // counts/mm
double CountsPerDegree      =  CountsPerDistance * trackWidth / 360 ;  // counts/degree

void displayspeed(void)
{
  uint8_t status1, status2, status3, status4;
  bool valid1, valid2, valid3, valid4;

  enc1 = roboclaw.ReadEncM1(address, &status1, &valid1);
  enc2 = roboclaw.ReadEncM2(address, &status2, &valid2);
  speed1 = roboclaw.ReadSpeedM1(address, &status3, &valid3);
  speed2 = roboclaw.ReadSpeedM2(address, &status4, &valid4);

  speed_error = speed1 - speed2;
  pos_error = enc1 - enc2;

  Serial.print("Encoder1:");
  Serial.print(enc1, DEC);
  Serial.print(" ");
  Serial.print(speed1, DEC);
  Serial.print("     ");

  Serial.print("Encoder2:");
  Serial.print(enc2, DEC);
  Serial.print(" ");
  Serial.print(speed2, DEC);
  Serial.print("      ");

  Serial.print("Error:");
  Serial.print(pos_error);
  Serial.print(" ");
  Serial.print(speed_error);
  Serial.println("      ");

  Dr = (enc2 - previousRightCount) / CountsPerDistance;
  previousRightCount = enc2;
  Dl = (enc1 - previousLeftCount) / CountsPerDistance;
  previousLeftCount = enc1;

  D = (Dr + Dl) / 2;
  xPos += D * cos(anglePos * PI / 180);
  yPos += D * sin(anglePos * PI / 180);

  difference = Dr - Dl;
  anglePos += asin(difference / encoderSeparation) * 180 / PI;

  Serial.print(xPos);
  Serial.print("     ");
  Serial.print(yPos);
  Serial.print("     ");
  Serial.println(anglePos);
}

void stop() {
  roboclaw.ForwardM1(address,0);
  roboclaw.ForwardM2(address,0);
  last_pos_error = 0;
  M2_error = 0;
  previousRightCount = 0;
  previousLeftCount = 0;
}

void motor_move(double distance) {

  if (distance >= 0) {
    foward = 1;
  }
  else {
    foward = 0;
  }
  distance = abs(distance * CountsPerDistance);
  Serial.println(distance);

  roboclaw.ResetEncoders(address);
  delay(10);

  if (foward == 1) {
    roboclaw.ForwardM1(address, contraint_speed);
    roboclaw.ForwardM2(address, contraint_speed);
    while (1) {
      displayspeed();
      M2_error += ((pos_error * kp) + ((pos_error - last_pos_error) * kd)) / 1000;
      last_pos_error = pos_error;
      roboclaw.ForwardM2(address, contraint_speed + M2_error);
      if (enc1 >= distance - tolerance) {
        roboclaw.ForwardM1(address, 0);
        roboclaw.ForwardM2(address, 0);
        delay(100);
        displayspeed();
        break;
      }
    }
  }

  else if (foward == 0) {
    roboclaw.BackwardM1(address, contraint_speed);
    roboclaw.BackwardM2(address, contraint_speed);
    while (1) {
      displayspeed();
      M2_error += ((pos_error * kp) + ((pos_error - last_pos_error) * kd)) / 1000;
      last_pos_error = pos_error;
      roboclaw.BackwardM2(address, contraint_speed - M2_error);
      if (enc1 <= -distance + tolerance) {
        roboclaw.BackwardM1(address, 0);
        roboclaw.BackwardM2(address, 0);
        delay(100);
        displayspeed();
        break;
      }
    }
  }
  last_pos_error = 0;
  M2_error = 0;
  previousRightCount = 0;
  previousLeftCount = 0;
}

void motor_rotate(float angle) {

  double angle_distance = abs(angle * CountsPerDegree);
  Serial.println(angle_distance);

  roboclaw.ResetEncoders(address);
  delay(10);

  if (angle >= 0) {
    roboclaw.BackwardM1(address, rotation_speed);
    roboclaw.ForwardM2(address, rotation_speed);
    while (1) {
      displayspeed();
      pos_error = -enc1 - enc2;
      M2_error += ((pos_error * kp) + ((pos_error - last_pos_error) * kd)) / 1000;
      last_pos_error = pos_error;
      roboclaw.ForwardM2(address, rotation_speed + M2_error);
      if (angle_distance < 200){
        if (enc1 <= -angle_distance+rotation_tolerance2){
          roboclaw.ForwardM1(address,0);
          roboclaw.BackwardM2(address,0);
          delay(100);
          displayspeed();
          break;
        }
      }
      else {
        if (enc1 <= -angle_distance+rotation_tolerance){
          roboclaw.ForwardM1(address,0);
          roboclaw.BackwardM2(address,0);
          delay(100);
          displayspeed();
          break;
        }
      }
    }
  }

  else if (angle < 0) {
    roboclaw.ForwardM1(address, rotation_speed);
    roboclaw.BackwardM2(address, rotation_speed);
    while (1) {
      displayspeed();
      pos_error = -enc1 - enc2;
      M2_error += ((pos_error * kp) + ((pos_error - last_pos_error) * kd)) / 1000;
      last_pos_error = pos_error;
      roboclaw.BackwardM2(address, rotation_speed - M2_error);
      if (angle_distance < 200){
        if (enc1 >= angle_distance-rotation_tolerance2){
          roboclaw.ForwardM1(address,0);
          roboclaw.BackwardM2(address,0);
          delay(100);
          displayspeed();
          break;
        }
      }
      else {
        if (enc1 >= angle_distance-rotation_tolerance){
          roboclaw.ForwardM1(address,0);
          roboclaw.BackwardM2(address,0);
          delay(100);
          displayspeed();
          break;
        }
      }
    }
  }
  last_pos_error = 0;
  M2_error = 0;
  previousRightCount = 0;
  previousLeftCount = 0;
}

void moveTo(double x, double y, bool direction = 1) {
  //  if (side) {
  //    y = -y;
  //  }

  xDest = x - xPos;
  yDest = y - yPos;
  Dest  = sqrt(sq(xDest) + sq(yDest)) ;
  angleDest = (atan2 (yDest, xDest )) * 180 / PI;
  double diff_angleDest = angleDest - anglePos;

  if (direction == 0) {
    angleDest = angleDest + 180;
    diff_angleDest = diff_angleDest > 180 ? diff_angleDest - 180 : diff_angleDest + 180;
    Dest = -Dest;
  }

  if (diff_angleDest > 180) {
    diff_angleDest -= 360 ;
  }
  else if (diff_angleDest < -180) {
    diff_angleDest += 360 ;
  }

  Serial.print(angleDest);
  Serial.print("     ");
  Serial.print(diff_angleDest);
  Serial.print("     ");
  Serial.println(Dest);

  while (diff_angleDest >= 1 || diff_angleDest <= -1) {
    motor_rotate(diff_angleDest);

    diff_angleDest = angleDest - anglePos;
    if (direction == 0) {
      angleDest = angleDest + 180;
      diff_angleDest = diff_angleDest > 180 ? diff_angleDest - 180 : diff_angleDest + 180;
      Dest = -Dest;
    }

    if (diff_angleDest > 180) {
      diff_angleDest -= 360 ;
    }
    else if (diff_angleDest < -180) {
      diff_angleDest += 360 ;
    }
  }

  delay(100);
  motor_move(Dest);

  //  xPos = x;
  //  yPos = y;
  //  anglePos = angleDest;
}

void rotate(double angle) {

  angleDest = angle;
  double diff_angleDest = angleDest - anglePos;

  if (diff_angleDest > 180) {
    diff_angleDest -= 360 ;
  }
  else if (diff_angleDest < -180) {
    diff_angleDest += 360 ;
  }

  Serial.print(angleDest);
  Serial.print("     ");
  Serial.println(diff_angleDest);

  while (diff_angleDest >= 1 || diff_angleDest <= -1) {
    motor_rotate(diff_angleDest);

    diff_angleDest = angleDest - anglePos;

    if (diff_angleDest > 180) {
      diff_angleDest -= 360 ;
    }
    else if (diff_angleDest < -180) {
      diff_angleDest += 360 ;
    }
  }

  anglePos = angleDest;
}

void resume() {
  if (!shouldResume) return;

  moveTo(xTarget, yTarget, targetDirection);
  rotate(aTarget);
}


//
// Check function
// Function passed from main to perform all necessary checks while stuck in while loop
//

void performChecks () {


  //
  // Ultras
  //

  if (obstacleDetection.shouldStop()) {

    // Stop motors
    stop();
    shouldResume = true;

    // Give Ultras enough time to send again
    delay(500);

    // Try again
    return;

  } else {

    // Otherwise, try to resume
    resume();
    shouldResume = false;
  }


  //
  // Timeout
  //

  if (millis() - startMillis > 90000) {
    stop();
    while(true);
  }
}


void setup() {
  Serial.begin(9600);
  pinMode(LIMIT_SWITCH_PIN, INPUT);

  // Start drivers
  roboclaw.begin(38400);
  roboclaw.ResetEncoders(address);
   

  //
  // Side pin
  //

  pinMode(SIDE_PIN, INPUT);
  if (digitalRead(SIDE_PIN)) {
    tableSide = Blue;
  } else {
    tableSide = Yellow;
  }
}

void loop() {


  //
  // Limit switch
  //

  if (!limitSwitchState) {

    // Get switch state
    limitSwitchState = digitalRead(LIMIT_SWITCH_PIN);

    // Return if it's still in
    return;
  }

  // Set startMillis if it's not set (should mean first loop)
  if (startMillis == 0) {
    startMillis = millis();
    Esc.begin();
  }

  //
  // Main code
  //
  
  contraint_speed = ramp_speed;
  moveTo(180, 450);
//  delay(200);

  contraint_speed = deramp_speed;
  moveTo(180, 800);
  delay(1000);

  contraint_speed = normal_speed;
  moveTo(540, 950);
  delay(200);
  moveTo(540, 870);
  delay(200);
  dumbo.down();
  Esc.Sucker(1);
  motor_move(-100);
  delay(200);
  moveTo(1000, 800);
  delay(200);
  moveTo(1400, 450);
  delay(200);
  moveTo(1400, 200);
  delay(200);
  rotate(-30);
  Esc.Sucker(1);
  motor_move(-100);
  delay(200);
  moveTo(1119, 274);
  delay(200);
  moveTo(689, 263);
  delay(200);
  moveTo(489, 178);
  delay(200);
  rotate(180);
  dumbo.up();
  Esc.Sucker(0);

  while(1);
}
