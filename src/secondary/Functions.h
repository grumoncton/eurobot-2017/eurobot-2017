#include "Arduino.h"
#include "math.h"
#include "Config.h"

class Functions{
  public:
    void moveTo(double _x, double _y){
      deltaX = _x - config.PosX;
      deltaY = _y - config.PosY;
      Heading = (atan2 ( deltaY, deltaX )) * 180 / PI;

    }
}
