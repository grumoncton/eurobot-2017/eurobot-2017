#include "Arduino.h"
#include "Grabber.h"
#include "Servo.h"

#define ANGLE_DOWN 0
#define ANGLE_UP 90
#define ANGLE_RIGHT_OPEN 56
#define ANGLE_LEFT_OPEN 120
#define ANGLE_RIGHT_SORT 0
#define ANGLE_LEFT_SORT 180
#define ANGLE_RIGHT_CLOSED 105
#define ANGLE_LEFT_CLOSED 75
#define ANGLE_RIGHT_HELP 120
#define ANGLE_LEFT_HELP 40

Grabber::Grabber(Servo _base, Servo _right, Servo _left) {
  base = _base;
  right = _right;
  left = _left;
}

void Grabber::lower() {
  base.write(ANGLE_DOWN);
}

void Grabber::raise() {
  base.write(ANGLE_UP);
}

void Grabber::close() {
  right.write(ANGLE_RIGHT_CLOSED);
  left.write(ANGLE_LEFT_CLOSED);
}

void Grabber::open() {
  right.write(ANGLE_RIGHT_OPEN);
  left.write(ANGLE_LEFT_OPEN);
}

void Grabber::sort(Side side) {
  switch (side) {
  case Right:
    right.write(ANGLE_RIGHT_SORT);
    left.write(ANGLE_LEFT_HELP);
    break;
  case Left:
    left.write(ANGLE_LEFT_SORT);
    right.write(ANGLE_RIGHT_HELP);
    break;
  }
  delay(600);
  close();
}

void Grabber::prepare() {
  lower();
  delay(LOWER_DELAY);
  open();
}

void Grabber::grab() {
  close();
  delay(CLOSE_DELAY);
  raise();
  delay(RAISE_DELAY);
}

void Grabber::pickup(Side side){
  base.write(ANGLE_DOWN);
  delay(LOWER_DELAY);
  right.write(ANGLE_RIGHT_OPEN);
  left.write(ANGLE_LEFT_OPEN);
  delay(OPEN_DELAY);
  right.write(ANGLE_RIGHT_CLOSED);
  left.write(ANGLE_LEFT_CLOSED);
  delay(CLOSE_DELAY);
  base.write(ANGLE_UP);
  delay(RAISE_DELAY);
  switch (side) {// Left même couleur (jaune) // Right 2 couleurs
  case Right:
    right.write(ANGLE_RIGHT_SORT);
    left.write(ANGLE_LEFT_HELP);
    break;
  case Left:
    left.write(ANGLE_LEFT_SORT);
    right.write(ANGLE_RIGHT_HELP);
    break;
  }
  delay(1000);
  right.write(ANGLE_RIGHT_CLOSED);
  left.write(ANGLE_LEFT_CLOSED);
}
