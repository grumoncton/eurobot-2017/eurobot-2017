#ifndef Grabber_h
#define Grabber_h

#include "Arduino.h"
#include "Servo.h"
#include "Side.h"

#define LOWER_DELAY 200
#define RAISE_DELAY 700
#define CLOSE_DELAY 200
#define OPEN_DELAY 1000

class Grabber {
public:
  Grabber(Servo base, Servo right, Servo left);
  void lower();
  void raise();
  void close();
  void open();
  void sort(Side side);
  void prepare();
  void grab();
  void pickup(Side side);
private:
  Servo base, right, left;
};

#endif
