#include "Arduino.h"
#include "Droppers.h"
#include "Servo.h"
;

#define ANGLE_CLOSED 0
#define ANGLE_OPEN 90
#define ANGLE_spin 180

Droppers::Droppers(Servo _servo) {
  servo = _servo;
}

void Droppers::spin() {
  servo.write(ANGLE_OPEN);
}

void Droppers::open() {
  servo.write(ANGLE_OPEN);
}

void Droppers::close() {
  servo.write(ANGLE_CLOSED);
}
