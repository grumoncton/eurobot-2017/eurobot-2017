#ifndef Dropper_h
#define Dropper_h

#include "Arduino.h"
#include "Servo.h"

class Droppers {
public:
  Droppers(Servo servo);
  void spin();
  void open();
  void close();
private:
  Servo servo;
};

#endif
