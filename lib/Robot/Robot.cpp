////See BareMinimum example for a list of library functions
//
////Includes required to use Roboclaw library
//#include "Arduino.h"
//#include "SoftwareSerial.h"
//#include "RoboClaw.h"
//#include "math.h"
//#include "Robot.h"
//#include "Direction.h"
//
//RoboClaw roboclaw(&Serial3,10000);
//#define address 0x80
//
//#define X_START 0
//#define Y_START 0
//#define A_START 90
//
//double    xPos      =  X_START;
//double    yPos      =  Y_START;
//double    anglePos  =  A_START;
//
//double    xDest      =  0;
//double    yDest      =  0;
//double    Dest       =  sqrt(sq(xDest) + sq(yDest)) ;
//double    angleDest  =  anglePos ;
//
//int32_t enc1             =  0;     // position left encoder
//int32_t enc2             =  0;     // position right encoder
//int32_t speed1           =  0;     // speed left encoder
//int32_t speed2           =  0;     // speed right encoder
//
//double Dr  =  0;
//double Dl  =  0;
//double D   =  0;
//
//double previousRightCount;
//double previousLeftCount;
//double difference;
//
//float speed_error     =  0;
//float pos_error       =  0;
//float last_pos_error  =  0;
//float M2_error        =  0;
//
//bool foward;
//double contraint_speed  =  20;  // Motor speed
//double normal_speed     =  20;
//double rotation_speed   =  30;
//double kp               =  1;
//double ki               =  1;
//double kd               =  15;
//double tolerance        = 350;
//
//double encoderSeparation    =  105;
//double trackWidth           =  PI * encoderSeparation;                 // mm/rotation
//double wheelPerimeter       =  200;                                    // mm/revolution
//double countsPerRevolution  =  4096 ;                                   // counts/revolution
//double CountsPerDistance    =  countsPerRevolution / wheelPerimeter ;  // counts/mm
//double CountsPerDegree      =  CountsPerDistance * trackWidth / 360 ;  // counts/degree
//
//Robot::Robot(HardwareSerial& ultraSerial): _ultraSerial(ultraSerial) { }
//
//void sleep(){
//  roboclaw.ForwardM1(address,0);
//  roboclaw.ForwardM2(address,0);
//  delay(600000);
//}
//
//void Robot::displayspeed() {
//  _performChecks();
//
//  uint8_t status1,status2,status3,status4;
//  bool valid1,valid2,valid3,valid4;
//
//  enc1 = roboclaw.ReadEncM1(address, &status1, &valid1);
//  enc2 = roboclaw.ReadEncM2(address, &status2, &valid2);
//  speed1 = roboclaw.ReadSpeedM1(address, &status3, &valid3);
//  speed2 = roboclaw.ReadSpeedM2(address, &status4, &valid4);
//
//  speed_error = speed1 - speed2;
//  pos_error = enc1 - enc2;
//
//  Serial.print("Encoder1:");
//    Serial.print(enc1,DEC);
//    Serial.print(" ");
//    Serial.print(speed1,DEC);
//    Serial.print("     ");
//
//  Serial.print("Encoder2:");
//    Serial.print(enc2,DEC);
//    Serial.print(" ");
//    Serial.print(speed2,DEC);
//    Serial.print("      ");
//
//    Serial.print("Error:");
//    Serial.print(pos_error);
//    Serial.print(" ");
//    Serial.print(speed_error);
//    Serial.println("      ");
//
//    Dr = (enc2 - previousRightCount) / CountsPerDistance;
//    previousRightCount = enc2;
//    Dl = (enc1 - previousLeftCount) / CountsPerDistance;
//    previousLeftCount = enc1;
//
//    D = (Dr + Dl) / 2;
//    xPos += D * cos(anglePos * PI / 180);
//    yPos += D * sin(anglePos * PI / 180);
//
//    difference = Dr - Dl;
//    anglePos += asin(difference / encoderSeparation) * 180 / PI;
//
////    Serial.print(xPos);
////    Serial.print("     ");
////    Serial.print(yPos);
////    Serial.print("     ");
////    Serial.println(anglePos);
//}
//
//void Robot::motor_move(double distance) {
//
//  if (distance >= 0) {
//    foward = 1;
//  }
//  else {
//    foward = 0;
//  }
//  distance = abs(distance * CountsPerDistance);
//  Serial.println(distance);
//
//  roboclaw.ResetEncoders(address);
//  delay(10);
//
//  if (foward == 1) {
//    roboclaw.ForwardM1(address,contraint_speed);
//    roboclaw.ForwardM2(address,contraint_speed);
//    while(1){
//      displayspeed();
//      M2_error += ((pos_error*kp) + ((pos_error-last_pos_error)*kd)) / 230;
//      last_pos_error = pos_error;
//      roboclaw.ForwardM2(address,contraint_speed + M2_error);
//      if (enc1 >= distance-tolerance){
//        roboclaw.ForwardM1(address,0);
//        roboclaw.ForwardM2(address,0);
//        delay(100);
//        displayspeed();
//        break;
//      }
//    }
//  }
//
//  else if (foward == 0) {
//    roboclaw.BackwardM1(address,contraint_speed);
//    roboclaw.BackwardM2(address,contraint_speed);
//    while(1){
//      displayspeed();
//      M2_error += ((pos_error*kp) + ((pos_error-last_pos_error)*kd)) / 300;
//      last_pos_error = pos_error;
//      roboclaw.BackwardM2(address,contraint_speed - M2_error);
//      if (enc1 <= -distance+tolerance){
//        roboclaw.BackwardM1(address,0);
//        roboclaw.BackwardM2(address,0);
//        delay(100);
//        displayspeed();
//        break;
//      }
//    }
//  }
//  last_pos_error = 0;
//  M2_error = 0;
//  previousRightCount = 0;
//  previousLeftCount = 0;
//}
//
//void Robot::motor_rotate(float angle) {
//
//  double angle_distance = abs(angle * CountsPerDegree);
//  Serial.println(angle_distance);
//
//  roboclaw.ResetEncoders(address);
//  delay(10);
//
//  if (angle >= 0) {
//    roboclaw.BackwardM1(address,rotation_speed);
//    roboclaw.ForwardM2(address,rotation_speed);
//    while(1){
//      displayspeed();
//      pos_error = -enc1 - enc2;
//      M2_error += ((pos_error*kp) + ((pos_error-last_pos_error)*kd)) / 1000;
//      last_pos_error = pos_error;
//      roboclaw.ForwardM2(address,rotation_speed + M2_error);
//      if (angle_distance < 200){
//        if (enc1 <= -angle_distance+50){
//          roboclaw.ForwardM1(address,0);
//          roboclaw.BackwardM2(address,0);
//          delay(100);
//          displayspeed();
//          break;
//        }
//      }
//      else {
//        if (enc1 <= -angle_distance+200){
//          roboclaw.ForwardM1(address,0);
//          roboclaw.BackwardM2(address,0);
//          delay(100);
//          displayspeed();
//          break;
//        }
//      }
//    }
//  }
//
//  else if (angle < 0) {
//    roboclaw.ForwardM1(address,rotation_speed);
//    roboclaw.BackwardM2(address,rotation_speed);
//   while(1){
//     displayspeed();
//      pos_error = -enc1 - enc2;
//      M2_error += ((pos_error*kp) + ((pos_error-last_pos_error)*kd)) / 1000;
//      last_pos_error = pos_error;
//      roboclaw.BackwardM2(address,rotation_speed - M2_error);
//      if (angle_distance < 200){
//        if (enc1 >= angle_distance-50){
//          roboclaw.ForwardM1(address,0);
//          roboclaw.BackwardM2(address,0);
//          delay(100);
//          displayspeed();
//          break;
//        }
//      }
//      else {
//        if (enc1 >= angle_distance-200){
//          roboclaw.ForwardM1(address,0);
//          roboclaw.BackwardM2(address,0);
//          delay(100);
//          displayspeed();
//          break;
//        }
//      }
//    }
//  }
//  last_pos_error = 0;
//  M2_error = 0;
//  previousRightCount = 0;
//  previousLeftCount = 0;
//}
//
//void Robot::moveTo(double x, double y, Direction direction)  {
////  if (side) {
////    y = -y;
////  }
//
//  xDest = x - xPos;
//  yDest = y - yPos;
//  Dest  = sqrt(sq(xDest) + sq(yDest)) ;
//  angleDest = (atan2 (yDest,xDest )) * 180 / PI;
//  double diff_angleDest = angleDest - anglePos;
//
//  switch (direction) {
//  case Forwards:
//    _ultraSerial.write('f');
//    break;
//  case Backwards:
//    _ultraSerial.write('b');
//
//    // Switch things based on side
//    angleDest += 180;
//    diff_angleDest = diff_angleDest > 180 ? diff_angleDest - 180 : diff_angleDest + 180;
//    Dest = -Dest;
//  }
//
//  if (diff_angleDest > 180) {
//    diff_angleDest -= 360 ;
//  }
//  else if (diff_angleDest < -180) {
//    diff_angleDest += 360 ;
//  }
//
//  Serial.print(angleDest);
//  Serial.print("     ");
//  Serial.print(diff_angleDest);
//  Serial.print("     ");
//  Serial.println(Dest);
//
//  while (diff_angleDest >= 1 || diff_angleDest <= -1) {
//    motor_rotate(diff_angleDest);
//
//    diff_angleDest = angleDest - anglePos;
//    if(direction == 0){
//      angleDest = angleDest+180;
//      diff_angleDest = diff_angleDest > 180 ? diff_angleDest - 180 : diff_angleDest + 180;
//      Dest = -Dest;
//    }
//
//    if (diff_angleDest > 180) {
//      diff_angleDest -= 360 ;
//    }
//    else if (diff_angleDest < -180) {
//      diff_angleDest += 360 ;
//    }
//  }
//
//  delay(100);
//  motor_move(Dest);
//
////  xPos = x;
////  yPos = y;
////  anglePos = angleDest;
//}
//
//void Robot::rotate(double angle) {
//
//  angleDest = angle;
//  double diff_angleDest = angleDest - anglePos;
//
//  while (diff_angleDest > 180) {
//    diff_angleDest -= 360 ;
//  }
//  while (diff_angleDest < -180) {
//    diff_angleDest += 360 ;
//  }
//
//  Serial.print(angleDest);
//  Serial.print("     ");
//  Serial.println(diff_angleDest);
//
//  while (diff_angleDest >= 1 || diff_angleDest <= -1) {
//    motor_rotate(diff_angleDest);
//
//    diff_angleDest = angleDest - anglePos;
//
//    if (diff_angleDest > 180) {
//      diff_angleDest -= 360 ;
//    }
//    else if (diff_angleDest < -180) {
//      diff_angleDest += 360 ;
//    }
//  }
//}
//
//void Robot::moveRelative(double x, double y, Direction direction) {
//
//  // Calculate differences
//  double diffX = x - xPos;
//  double diffY = y - yPos;
//
//  // Calculate target angle
//  double targetA = atan(diffY / diffX);
//  if (diffY < 0) targetA = - targetA;
//
//  // How far away are we?
//  double dist = sqrt(sq(diffX) + sq(diffY));
//
//  // Calculate target coords
//  double targetX = xPos + dist * cos(targetA);
//  double targetY = yPos + dist * sin(targetA);
//
//  moveTo(targetX, targetY, direction);
//}
//
//void Robot::begin() {
//  //Open Serial and roboclaw serial ports
//  roboclaw.begin(38400);
//  roboclaw.ResetEncoders(address);
//}
//
//void Robot::stop() {
//  roboclaw.ForwardM1(address,0);
//  roboclaw.ForwardM2(address,0);
//  last_pos_error = 0;
//  M2_error = 0;
//  previousRightCount = 0;
//  previousLeftCount = 0;
//}
//
//void Robot::resume() {
//  if (!shouldResume) return;
//
//  moveTo(xTarget, yTarget, targetDirection);
//  rotate(aTarget);
//}
//
