#ifndef Robot_h
#define Robot_h
struct Robot {
  double xPos, yPos, aPos;
};
//#include "math.h"
//#include "Direction.h"
//#include "RoboClaw.h"
//
//#define DEBUG
//
//#define ADDRESS 0x80
//#define MAX_SPEED 20
//#define ROTATION_SPEED 30
//#define TOLERANCE 350
//#define ROTATION_TOLERANCE 3
//
#define CONE_CENTRE_TO_ROBOT_CENTRE 158.5f
#define OFFSET_RAIL_TO_ROBOT 30
#define HALF_RAIL_WIDTH 68
#define ROBOT_CENTRE_TO_ROBOT_EDGE 105.08f
//
//class Robot {
//public:
//  Robot(HardwareSerial& ultraSerial);
//  double xPos, yPos, aPos;
//  double aPosRad;
//  void begin();
//  void moveTo(double x, double y, Direction direction = Forwards);
//  void rotate(double angle);
//  void moveRelative(double x, double y, Direction direction);
//  void stop();
//  void resume();
//
//  // Function passed from main to perform all necessary checks while stuck in while loop
//  void (* _performChecks) ();
//  bool shouldResume;
//  void displayspeed();
//
//private:
//
//  double xDiff, yDiff, aDiff, diff;
//  double Dr, Dl, D, difference;
//  double xTarget, yTarget, aTarget;
//  Direction targetDirection;
//  int32_t enc1;
//  int32_t enc2;
//  int32_t speed1;
//  int32_t speed2;
//  double speed_error, pos_error, last_pos_error, M2_error;
//  long previousRightCount, previousLeftCount;
//  Direction direction = Forwards;
//  double kp = 1;
//  double ki = 1;
//  double kd = 15;
//
//  const double encoderSeparation    =  105;
//  const double trackWidth           =  PI * encoderSeparation;                 // mm/rotation
//  const double wheelPerimeter       =  200;                                    // mm/revolution
//  const double countsPerRevolution  =  4096 ;                                  // counts/revolution
//  const double CountsPerDistance    =  countsPerRevolution / wheelPerimeter ;  // counts/mm
//  const double CountsPerDegree      =  CountsPerDistance * trackWidth / 360 ;  // counts/degree
//
//  void _calcError();
//  void motor_move(double distance);
//  void motor_rotate(float angle);
//
//  HardwareSerial& _ultraSerial;
//};
//
#endif
