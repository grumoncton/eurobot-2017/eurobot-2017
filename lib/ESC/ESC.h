#ifndef ESC_h
#define ESC_h

#include "Servo.h"

class ESC
{
    public:
      void begin();
      void Sucker(bool ChangeRelay);
      void write_esc(int throttle);

    private:
      void changeThrottle(int throttle);
      int readThrottle();
      int normalizeThrottle(int value);
};

#endif
