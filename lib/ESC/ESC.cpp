#include "Arduino.h"
#include "Servo.h"
#include "ESC.h"

#define RELAY1      22 
#define RELAY2      23
#define BLOW_SPEED  110
#define SUCK_SPEED  180

Servo esc;

int escPin = 46;
int minPulseRate = 1500;
int maxPulseRate = 2000;
int throttleChangeDelay = 100;
int duration = 5000;
int throttle;


void ESC::begin() {
  pinMode(RELAY1, OUTPUT);   
  pinMode(RELAY2, OUTPUT); 
  esc.attach(escPin, minPulseRate, maxPulseRate); 

  esc.write(0);
}

void ESC::Sucker( bool ChangeRelay) {  // 1-Blow & 0-Suck
    
  if (ChangeRelay == 1) {
    digitalWrite(RELAY1,1);  //Clockwise      
    digitalWrite(RELAY2,1);
    throttle = normalizeThrottle(SUCK_SPEED);
   // throttle = SUCK_SPEED;
    changeThrottle(throttle);
    Serial.println("Light ON");
    delay(5000);  
    throttle = normalizeThrottle(0);
   // throttle = SUCK_SPEED;
    changeThrottle(throttle);                                      
  }
  else {            
    digitalWrite(RELAY1,0); //Anti-clockwise
    digitalWrite(RELAY2,0);
     throttle = normalizeThrottle(BLOW_SPEED);
   // throttle = BLOW_SPEED;
    changeThrottle(throttle);
    throttle = BLOW_SPEED; 
    Serial.println("Light OFF");
    delay(5000);
     throttle = normalizeThrottle(0);
   // throttle = BLOW_SPEED;
    changeThrottle(throttle);
  }  
}

void ESC::changeThrottle(int throttle) {
  
  // Read the current throttle value
  int currentThrottle = readThrottle();
  
  // Increase speed !!!
  int step = 5;
  if( throttle < currentThrottle )
    step = -5;
  
  // Slowly move to the new throttle value 
  while( currentThrottle != throttle ) {
    esc.write(currentThrottle + step);
    currentThrottle = readThrottle();
    delay(throttleChangeDelay);
  }
}

int ESC::readThrottle() {
  int throttle = esc.read();
  
  Serial.print("Current throttle is: ");
  Serial.println(throttle);
  
  return throttle;
}

// Ensure the throttle value is between 0 - 180
int ESC::normalizeThrottle(int value) {
  if( value < 0 )
    return 0;
  if( value > 180 )
    return 180;
  return value;
}

void ESC::write_esc(int throttle) {
  esc.write(throttle);
}

