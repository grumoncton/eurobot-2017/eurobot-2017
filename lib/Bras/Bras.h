#ifndef Bras_h
#define Bras_h

#include <Servo.h>
#include "Arduino.h"
#include "Side.h"
#include "TableSide.h"
#include "Color.h"

#define INIT_POS_CYL_LEFT 0
#define TURN_CYL_LEFT 180
#define DEPO_CYL_LEFT 90
#define INIT_POS_OPEN_CLOSE_LEFT 180

#define DetectOpenClose_Left 85
#define DetectOpenClose_Right 95

#define spinDegree 75

#define ORANGE_SMALL_TURN 135
#define ORANGE_BIG_TURN 0
#define VALUE 180

#define INIT_POS_CYL_RIGHT 180
#define TURN_CYL_RIGHT 0
#define DEPO_CYL_RIGHT 90
#define INIT_POS_OPEN_CLOSE_RIGHT 0



class Bras {
public:
  Bras(Servo rightCyl, Servo rightOpenClose, Servo rightSpin, Servo leftCyl, Servo leftOpenClose, Servo leftSpin, int rightSpinPin, int leftSpinPin, Color color);

  void MovementSameColor_RIGHT();
  void MovementDifferentColor_RIGHT();

  void spinRight();
  void spinLeft();

  void MovementSameColor_LEFT();
  void MovementDifferentColor_LEFT();

  void DetectColor_Right(bool); // 0 jaune 1 bleu
  void DetectColor_Left(bool);  // 0 jaune 1 bleu

  void Deposition_Right();
  void Deposition_Left();

  void drop(int numColours);

  TableSide tableSide;
private:
  Servo _rightCyl, _rightOpenClose, _rightSpin, _leftCyl, _leftOpenClose, _leftSpin;
  int _rightSpinPin, _leftSpinPin;
  Color color;
};

#endif
