#include "Arduino.h"
#include "Bras.h"
#include "Servo.h"
#include "TableSide.h"
#include "Color.h"

Bras::Bras(Servo rightCyl, Servo rightOpenClose, Servo rightSpin, Servo leftCyl, Servo leftOpenClose, Servo leftSpin, int rightSpinPin, int leftSpinPin, Color _color){
  _rightCyl = _rightCyl;
  _rightOpenClose = _rightOpenClose;
  _rightSpin = _rightSpin;
  _leftCyl = _leftCyl;
  _leftOpenClose = _leftOpenClose;
  _leftSpin = _leftSpin;
  _rightSpinPin = rightSpinPin;
  _leftSpinPin = leftSpinPin;
  color = _color;
}

void Bras::MovementSameColor_RIGHT() {

  _rightCyl.write(INIT_POS_CYL_RIGHT-180);
  delay(1000);
  _rightCyl.write(INIT_POS_CYL_RIGHT);
  _rightOpenClose.write(135);
  delay(1000);
  _rightOpenClose.write(INIT_POS_OPEN_CLOSE_RIGHT);
}

void Bras::MovementDifferentColor_RIGHT() {
  _rightCyl.write(INIT_POS_CYL_RIGHT-180);
  delay(1000);
  _rightCyl.write(INIT_POS_CYL_RIGHT-70);
  _rightOpenClose.write(95);
  delay(1000);
  _rightOpenClose.write(INIT_POS_OPEN_CLOSE_RIGHT-180);
  _rightCyl.write(INIT_POS_CYL_RIGHT);
}

void Bras::MovementSameColor_LEFT() {
  _leftCyl.write(INIT_POS_CYL_LEFT+180);
  delay(1000);
  _leftCyl.write(INIT_POS_CYL_LEFT);
  _leftOpenClose.write(180-135);
  delay(1000);
  _leftOpenClose.write(INIT_POS_OPEN_CLOSE_LEFT);
}

void Bras::MovementDifferentColor_LEFT(){
  _leftCyl.write(INIT_POS_CYL_LEFT+180);
  delay(1000);
  _leftCyl.write(INIT_POS_CYL_LEFT+70);
  _leftOpenClose.write(85);
  delay(1000);
  _leftCyl.write(INIT_POS_CYL_LEFT+180);
  _leftOpenClose.write(INIT_POS_OPEN_CLOSE_LEFT);
}


void Bras::DetectColor_Right(bool i){ // 0 jaune 1 bleu
  switch (i) {
    case 1: // côté droit same color
      _rightSpin.attach(39);
      _rightSpin.write(spinDegree);
      _rightOpenClose.write(DetectOpenClose_Right);
      color.wait_color(1); // 1 jaune
      _rightSpin.detach();
      break;
    case 0:// coté gauche same color
      _rightSpin.attach(39);
      _rightSpin.write(spinDegree);
      _rightOpenClose.write(DetectOpenClose_Right);
      color.wait_color(0); // 0 bleu
      _rightSpin.detach();
      break;
    }
    _rightOpenClose.write(INIT_POS_OPEN_CLOSE_RIGHT);
}

void Bras::DetectColor_Left(bool i){ // 0 jaune 1 bleu
  switch (i) {
    case 1: // côté droit same color
      _leftSpin.attach(38);
      _leftSpin.write(0);
      _leftOpenClose.write(DetectOpenClose_Left);
      color.wait_color(1); // 1 jaune
      _leftSpin.detach();
      break;
    case 0:// coté gauche same color
      _leftSpin.attach(38);
      _leftSpin.write(180);
      _leftOpenClose.write(DetectOpenClose_Left);
      color.wait_color(0); // 0 bleu
      _leftSpin.detach();
      break;
    }
    _leftOpenClose.write(INIT_POS_OPEN_CLOSE_LEFT);
}

void Bras::spinRight(){
  _rightSpin.attach(_rightSpinPin);
  _rightSpin.write(spinDegree);
  _rightSpin.detach();
}

void Bras::spinLeft(){
  _leftSpin.attach(_leftSpinPin);
  _leftSpin.write(spinDegree);
  _leftSpin.detach();
}

void Bras::Deposition_Right(){
  _rightCyl.write(TURN_CYL_RIGHT);
  delay(1000);
  _rightCyl.write(DEPO_CYL_RIGHT);
  delay(500);
  _rightCyl.write(TURN_CYL_RIGHT);
}

void Bras::Deposition_Left(){
  _leftCyl.write(TURN_CYL_LEFT);
  delay(1000);
  _leftCyl.write(DEPO_CYL_LEFT);
  delay(500);
  _leftCyl.write(TURN_CYL_LEFT);
}

void Bras::drop(int numColours) {

  switch (numColours) {
    case 1:
      switch (tableSide) {
        case Blue: MovementSameColor_RIGHT();
        case Yellow: MovementSameColor_LEFT();
      }
    case 2:
      switch (tableSide) {
        case Blue: MovementDifferentColor_LEFT();
        case Yellow: MovementDifferentColor_RIGHT();
      }
  }
}

