#include "RoboClaw2.h"

int serialID;

//
// Constructor
//
RoboClaw2::RoboClaw2(uint8_t receivePin, uint8_t transmitPin, int serial) : BMSerial::BMSerial(receivePin, transmitPin)
{
	serialID = serial;
}

//
// Destructor
//
RoboClaw2::~RoboClaw2()
{
}

void RoboClaw2::write_n(uint8_t cnt, ... )
  {
	uint8_t crc=0;
	
	
	//send data with crc
	va_list marker;
	va_start( marker, cnt );     /* Initialize variable arguments. */
	for(uint8_t index=0;index<cnt;index++){
		uint8_t data = va_arg(marker, uint16_t);
		crc+=data;
		write(data);
	}
	va_end( marker );              /* Reset variable arguments.      */
	write(crc&0x7F);
}

void RoboClaw2::ForwardM1(uint8_t address, uint8_t speed){
	mega_write_n(3,address,M1FORWARD,speed);
}

void RoboClaw2::BackwardM1(uint8_t address, uint8_t speed){
	mega_write_n(3,address,M1BACKWARD,speed);
}

void RoboClaw2::SetMinVoltageMainBattery(uint8_t address, uint8_t voltage){
	mega_write_n(3,address,SETMINMB,voltage);
}

void RoboClaw2::SetMaxVoltageMainBattery(uint8_t address, uint8_t voltage){
	mega_write_n(3,address,SETMAXMB,voltage);
}

void RoboClaw2::ForwardM2(uint8_t address, uint8_t speed){
	mega_write_n(3,address,M2FORWARD,speed);
}

void RoboClaw2::BackwardM2(uint8_t address, uint8_t speed){
	mega_write_n(3,address,M2BACKWARD,speed);
}

void RoboClaw2::ForwardBackwardM1(uint8_t address, uint8_t speed){
	mega_write_n(3,address,M17BIT,speed);
}

void RoboClaw2::ForwardBackwardM2(uint8_t address, uint8_t speed){
	mega_write_n(3,address,M27BIT,speed);
}

void RoboClaw2::ForwardMixed(uint8_t address, uint8_t speed){
	mega_write_n(3,address,MIXEDFORWARD,speed);
}

void RoboClaw2::BackwardMixed(uint8_t address, uint8_t speed){
	mega_write_n(3,address,MIXEDBACKWARD,speed);
}

void RoboClaw2::TurnRightMixed(uint8_t address, uint8_t speed){
	mega_write_n(3,address,MIXEDRIGHT,speed);
}

void RoboClaw2::TurnLeftMixed(uint8_t address, uint8_t speed){
	mega_write_n(3,address,MIXEDLEFT,speed);
}

void RoboClaw2::ForwardBackwardMixed(uint8_t address, uint8_t speed){
	mega_write_n(3,address,MIXEDFB,speed);
}

void RoboClaw2::LeftRightMixed(uint8_t address, uint8_t speed){
	mega_write_n(3,address,MIXEDLR,speed);
}

uint32_t RoboClaw2::Read4_1(uint8_t address, uint8_t cmd, uint8_t *status,bool *valid){
	uint8_t crc;
	write(address);
	crc=address;
		
	write(cmd);
	crc+=cmd;

	uint32_t value;
	uint8_t data = read(10000);
	crc+=data;
	value=(uint32_t)data<<24;

	data = read(10000);
	crc+=data;
	value|=(uint32_t)data<<16;

	data = read(10000);
	crc+=data;
	value|=(uint32_t)data<<8;

	data = read(10000);
	crc+=data;
	value|=(uint32_t)data;
	
	data = read(10000);
	crc+=data;
	if(status)
		*status = data;
		
	data = read(10000);
	if(valid)
		*valid = ((crc&0x7F)==data);
		
	return value;
}

uint32_t RoboClaw2::ReadEncM1(uint8_t address, uint8_t *status,bool *valid){
	return mega_Read4_1(address,GETM1ENC,status,valid);
}

uint32_t RoboClaw2::ReadEncM2(uint8_t address, uint8_t *status,bool *valid){
	return mega_Read4_1(address,GETM2ENC,status,valid);
}

uint32_t RoboClaw2::ReadSpeedM1(uint8_t address, uint8_t *status,bool *valid){
	return mega_Read4_1(address,GETM1SPEED,status,valid);
}

uint32_t RoboClaw2::ReadSpeedM2(uint8_t address, uint8_t *status,bool *valid){
	return mega_Read4_1(address,GETM2SPEED,status,valid);
}

void RoboClaw2::ResetEncoders(uint8_t address){
	mega_write_n(2,address,RESETENC);
}

bool RoboClaw2::ReadVersion(uint8_t address,char *version){
	uint8_t crc;
	write(address);
	crc=address;
	write(GETVERSION);
	crc+=GETVERSION;
	
	for(uint8_t i=0;i<32;i++){
		version[i]=read(10000);
		crc+=version[i];
		if(version[i]==0){
			if((crc&0x7F)==read(10000))
				return true;
			else
				return false;
		}
	}
	return false;
}

uint16_t RoboClaw2::Read2(uint8_t address,uint8_t cmd,bool *valid){
	uint8_t crc;
	write(address);
	crc=address;
	write(cmd);
	crc+=cmd;
	
	uint16_t value;	
	uint8_t data = read(10000);
	crc+=data;
	value=(uint16_t)data<<8;

	data = read(10000);
	crc+=data;
	value|=(uint16_t)data;
	
	data = read(10000);
	if(valid)
		*valid = ((crc&0x7F)==data);
		
	return value;
}

uint16_t RoboClaw2::ReadMainBatteryVoltage(uint8_t address,bool *valid){
	return mega_Read2(address,GETMBATT,valid);
}

uint16_t RoboClaw2::ReadLogicBattVoltage(uint8_t address,bool *valid){
	return mega_Read2(address,GETLBATT,valid);
}

void RoboClaw2::SetMinVoltageLogicBattery(uint8_t address, uint8_t voltage){
	mega_write_n(3,address,SETMINLB,voltage);
}

void RoboClaw2::SetMaxVoltageLogicBattery(uint8_t address, uint8_t voltage){
	mega_write_n(3,address,SETMAXLB,voltage);
}

#define SetDWORDval(arg) (uint8_t)(arg>>24),(uint8_t)(arg>>16),(uint8_t)(arg>>8),(uint8_t)arg
#define SetWORDval(arg) (uint8_t)(arg>>8),(uint8_t)arg

void RoboClaw2::SetM1Constants(uint8_t address, uint32_t kd, uint32_t kp, uint32_t ki, uint32_t qpps){
	mega_write_n(18,address,SETM1PID,SetDWORDval(kd),SetDWORDval(kp),SetDWORDval(ki),SetDWORDval(qpps));
}

void RoboClaw2::SetM2Constants(uint8_t address, uint32_t kd, uint32_t kp, uint32_t ki, uint32_t qpps){
	mega_write_n(18,address,SETM2PID,SetDWORDval(kd),SetDWORDval(kp),SetDWORDval(ki),SetDWORDval(qpps));
}

uint32_t RoboClaw2::ReadISpeedM1(uint8_t address,uint8_t *status,bool *valid){
	return mega_Read4_1(address,GETM1ISPEED,status,valid);
}

uint32_t RoboClaw2::ReadISpeedM2(uint8_t address,uint8_t *status,bool *valid){
	return mega_Read4_1(address,GETM2ISPEED,status,valid);
}

void RoboClaw2::DutyM1(uint8_t address, uint16_t duty){
	mega_write_n(4,address,M1DUTY,SetWORDval(duty));
}

void RoboClaw2::DutyM2(uint8_t address, uint16_t duty){
	mega_write_n(4,address,M2DUTY,SetWORDval(duty));
}

void RoboClaw2::DutyM1M2(uint8_t address, uint16_t duty1, uint16_t duty2){
	mega_write_n(6,address,MIXEDDUTY,SetWORDval(duty1),SetWORDval(duty2));
}

void RoboClaw2::SpeedM1(uint8_t address, uint32_t speed){
	mega_write_n(6,address,M1SPEED,SetDWORDval(speed));
}

void RoboClaw2::SpeedM2(uint8_t address, uint32_t speed){
	mega_write_n(6,address,M2SPEED,SetDWORDval(speed));
}

void RoboClaw2::SpeedM1M2(uint8_t address, uint32_t speed1, uint32_t speed2){
	mega_write_n(10,address,M1SPEED,SetDWORDval(speed1),SetDWORDval(speed2));
}

void RoboClaw2::SpeedAccelM1(uint8_t address, uint32_t accel, uint32_t speed){
	mega_write_n(10,address,M1SPEEDACCEL,SetDWORDval(accel),SetDWORDval(speed));
}

void RoboClaw2::SpeedAccelM2(uint8_t address, uint32_t accel, uint32_t speed){
	mega_write_n(10,address,M2SPEEDACCEL,SetDWORDval(accel),SetDWORDval(speed));
}
void RoboClaw2::SpeedAccelM1M2(uint8_t address, uint32_t accel, uint32_t speed1, uint32_t speed2){
	mega_write_n(10,address,MIXEDSPEEDACCEL,SetDWORDval(accel),SetDWORDval(speed1),SetDWORDval(speed2));
}

void RoboClaw2::SpeedDistanceM1(uint8_t address, uint32_t speed, uint32_t distance, uint8_t flag){
	mega_write_n(11,address,M1SPEEDDIST,SetDWORDval(speed),SetDWORDval(distance),flag);
}

void RoboClaw2::SpeedDistanceM2(uint8_t address, uint32_t speed, uint32_t distance, uint8_t flag){
	mega_write_n(11,address,M2SPEEDDIST,SetDWORDval(speed),SetDWORDval(distance),flag);
}

void RoboClaw2::SpeedDistanceM1M2(uint8_t address, uint32_t speed1, uint32_t distance1, uint32_t speed2, uint32_t distance2, uint8_t flag){
	mega_write_n(19,address,M1SPEEDDIST,SetDWORDval(speed2),SetDWORDval(distance1),SetDWORDval(speed2),SetDWORDval(distance2),flag);
}

void RoboClaw2::SpeedAccelDistanceM1(uint8_t address, uint32_t accel, uint32_t speed, uint32_t distance, uint8_t flag){
	mega_write_n(15,address,M1SPEEDACCELDIST,SetDWORDval(accel),SetDWORDval(speed),SetDWORDval(distance),flag);
}

void RoboClaw2::SpeedAccelDistanceM2(uint8_t address, uint32_t accel, uint32_t speed, uint32_t distance, uint8_t flag){
	mega_write_n(15,address,M2SPEEDACCELDIST,SetDWORDval(accel),SetDWORDval(speed),SetDWORDval(distance),flag);
}

void RoboClaw2::SpeedAccelDistanceM1M2(uint8_t address, uint32_t accel, uint32_t speed1, uint32_t distance1, uint32_t speed2, uint32_t distance2, uint8_t flag){
	mega_write_n(23,address,M1SPEEDACCELDIST,SetDWORDval(accel),SetDWORDval(speed1),SetDWORDval(distance1),SetDWORDval(speed2),SetDWORDval(distance2),flag);
}

bool RoboClaw2::ReadBuffers(uint8_t address, uint8_t &depth1, uint8_t &depth2){
	bool valid;
	uint16_t value = mega_Read2(address,GETBUFFERS,&valid);
	if(valid){
		depth1 = value>>8;
		depth2 = value;
	}
	return valid;
}

bool RoboClaw2::ReadCurrents(uint8_t address, uint8_t &current1, uint8_t &current2){
	bool valid;
	uint16_t value = mega_Read2(address,GETCURRENTS,&valid);
	if(valid){
		current1 = value>>8;
		current2 = value;
	}
	return valid;
}

void RoboClaw2::SpeedAccelM1M2_2(uint8_t address, uint32_t accel1, uint32_t speed1, uint32_t accel2, uint32_t speed2){
	mega_write_n(18,address,MIXEDSPEED2ACCEL,SetDWORDval(accel1),SetDWORDval(speed1),SetDWORDval(accel2),SetDWORDval(speed2));
}

void RoboClaw2::SpeedAccelDistanceM1M2_2(uint8_t address, uint32_t accel1, uint32_t speed1, uint32_t distance1, uint32_t accel2, uint32_t speed2, uint32_t distance2, uint8_t flag){
	mega_write_n(27,address,MIXEDSPEED2ACCELDIST,SetDWORDval(accel1),SetDWORDval(speed1),SetDWORDval(distance1),SetDWORDval(accel2),SetDWORDval(speed2),SetDWORDval(distance2),flag);
}

void RoboClaw2::DutyAccelM1(uint8_t address, uint16_t duty, uint16_t accel){
	mega_write_n(6,address,M1DUTY,SetWORDval(duty),SetWORDval(accel));
}

void RoboClaw2::DutyAccelM2(uint8_t address, uint16_t duty, uint16_t accel){
	mega_write_n(6,address,M2DUTY,SetWORDval(duty),SetWORDval(accel));
}

void RoboClaw2::DutyAccelM1M2(uint8_t address, uint16_t duty1, uint16_t accel1, uint16_t duty2, uint16_t accel2){
	mega_write_n(10,address,MIXEDDUTY,SetWORDval(duty1),SetWORDval(accel1),SetWORDval(duty2),SetWORDval(accel2));
}

uint8_t RoboClaw2::ReadError(uint8_t address,bool *valid){
	uint8_t crc;
	write(address);
	crc=address;
	write(GETERROR);
	crc+=GETERROR;
	
	uint8_t value = read(10000);
	crc+=value;

	if(valid)
		*valid = ((crc&0x7F)==read(10000));
	else
		read(10000);
		
	return value;
}

void RoboClaw2::WriteNVM(uint8_t address){
	mega_write_n(2,address,WRITENVM);
}

//----------------------------------------------------------------------------------------
// MEGA STYLE
//----------------------------------------------------------------------------------------

uint32_t RoboClaw2::mega_Read4_1(uint8_t address, uint8_t cmd, uint8_t *status,bool *valid){
	uint8_t crc;
	switch(serialID){
		case(0):
			Serial.write(address);
			Serial.write(cmd);
			break;
		case(1):
			Serial1.write(address);
			Serial1.write(cmd);
			break;
		case(2):
			Serial2.write(address);
			Serial2.write(cmd);
			break;
		case(3):
			Serial3.write(address);
			Serial3.write(cmd);
			break;
	}
	crc=address;
	crc+=cmd;

	uint32_t value;
	uint8_t data;
	switch(serialID){
		case(0):
			data = Serial.read();
			break;
		case(1):
			data = Serial1.read();
			break;
		case(2):
			data = Serial2.read();
			break;
		case(3):
			data = Serial3.read();
			break;
	}
	crc+=data;
	value=(uint32_t)data<<24;

	switch(serialID){
		case(0):
			data = Serial.read();
			break;
		case(1):
			data = Serial1.read();
			break;
		case(2):
			data = Serial2.read();
			break;
		case(3):
			data = Serial3.read();
			break;
	}
	crc+=data;
	value|=(uint32_t)data<<16;

	switch(serialID){
		case(0):
			data = Serial.read();
			break;
		case(1):
			data = Serial1.read();
			break;
		case(2):
			data = Serial2.read();
			break;
		case(3):
			data = Serial3.read();
			break;
	}
	crc+=data;
	value|=(uint32_t)data<<8;

	switch(serialID){
		case(0):
			data = Serial.read();
			break;
		case(1):
			data = Serial1.read();
			break;
		case(2):
			data = Serial2.read();
			break;
		case(3):
			data = Serial3.read();
			break;
	}
	crc+=data;
	value|=(uint32_t)data;

	switch(serialID){
		case(0):
			data = Serial.read();
			break;
		case(1):
			data = Serial1.read();
			break;
		case(2):
			data = Serial2.read();
			break;
		case(3):
			data = Serial3.read();
			break;
	}
	crc+=data;
	if(status)
		*status = data;

	switch(serialID){
		case(0):
			data = Serial.read();
			break;
		case(1):
			data = Serial1.read();
			break;
		case(2):
			data = Serial2.read();
			break;
		case(3):
			data = Serial3.read();
			break;
	}
	if(valid)
		*valid = ((crc&0x7F)==data);
		
	return value;
}
bool RoboClaw2::mega_ReadVersion(uint8_t address,char *version){
	uint8_t crc;
	switch(serialID){
		case(0):
			Serial.write(address);
			Serial.write(GETVERSION);
			break;
		case(1):
			Serial1.write(address);
			Serial1.write(GETVERSION);
			break;
		case(2):
			Serial2.write(address);
			Serial2.write(GETVERSION);
			break;
		case(3):
			Serial3.write(address);
			Serial3.write(GETVERSION);
			break;
	}
	crc=address;
	crc+=GETVERSION;
	
	uint8_t input;
	for(uint8_t i=0;i<32;i++){
		switch(serialID){
			case(0):
				version[i] = Serial.read();
				break;
			case(1):
				version[i]= Serial1.read();
				break;
			case(2):
				version[i]= Serial2.read();
				break;
			case(3):
				version[i]= Serial3.read();
				break;
		}
		crc+=version[i];
		if(version[i]==0){
			switch(serialID){
				case(0):
					input= Serial.read();
					break;
				case(1):
					input= Serial1.read();
					break;
				case(2):
					input= Serial2.read();
					break;
				case(3):
					input= Serial3.read();
					break;
			}
			if((crc&0x7F)==input)
				return true;
			else
				return false;
		}
	}
	return false;
}

uint16_t RoboClaw2::mega_Read2(uint8_t address,uint8_t cmd,bool *valid){
	uint8_t crc;
	switch(serialID){
		case(0):
			Serial.write(address);
			Serial.write(cmd);
			break;
		case(1):
			Serial1.write(address);
			Serial1.write(cmd);
			break;
		case(2):
			Serial2.write(address);
			Serial2.write(cmd);
			break;
		case(3):
			Serial3.write(address);
			Serial3.write(cmd);
			break;
	}
	crc=address;
	crc+=cmd;
	
	uint16_t value;	
	uint8_t data;
	switch(serialID){
		case(0):
			data = Serial.read();
			break;
		case(1):
			data = Serial1.read();
			break;
		case(2):
			data = Serial2.read();
			break;
		case(3):
			data = Serial3.read();
			break;
	}
	crc+=data;
	value=(uint16_t)data<<8;

	switch(serialID){
		case(0):
			data = Serial.read();
			break;
		case(1):
			data = Serial1.read();
			break;
		case(2):
			data = Serial2.read();
			break;
		case(3):
			data = Serial3.read();
			break;
	}
	crc+=data;
	value|=(uint16_t)data;

	switch(serialID){
		case(0):
			data = Serial.read();
			break;
		case(1):
			data = Serial1.read();
			break;
		case(2):
			data = Serial2.read();
			break;
		case(3):
			data = Serial3.read();
			break;
	}
	if(valid)
		*valid = ((crc&0x7F)==data);
		
	return value;
}

void RoboClaw2::mega_write_n(uint8_t cnt, ... )
  {
	uint8_t crc=0;
	uint8_t data[cnt+1];
	
	//send data with crc
	va_list marker;
	va_start( marker, cnt );     /* Initialize variable arguments. */
	for(uint8_t index=0;index<cnt;index++){
		data[index] = va_arg(marker, uint16_t);
		crc+=data[index];
	}
	va_end( marker );              /* Reset variable arguments.      */
	data[cnt] = crc&0x7F;
	//Serial.println(crc&0x7F);
	switch(serialID){
		case(0):
			Serial.write(data, cnt+1);
			break;
		case(1):
			Serial1.write(data,cnt+1);
			break;
		case(2):
			Serial2.write(data, cnt+1);
			break;
		case(3):
			Serial3.write(data, cnt+1);
			break;
	}
//	write(crc&0x7F);
}

uint8_t RoboClaw2::mega_ReadError(uint8_t address,bool *valid){
	uint8_t crc;
	switch(serialID){
		case(0):
			Serial.write(address);
			Serial.write(GETERROR);
			break;
		case(1):
			Serial1.write(address);
			Serial1.write(GETERROR);
			break;
		case(2):
			Serial2.write(address);
			Serial2.write(GETERROR);
			break;
		case(3):
			Serial3.write(address);
			Serial3.write(GETERROR);
			break;
	}
	crc=address;
	crc+=GETERROR;
	
	uint8_t value;
	switch(serialID){
		case(0):
			value= Serial.read();
			break;
		case(1):
			value= Serial1.read();
			break;
		case(2):
			value= Serial2.read();
			break;
		case(3):
			value= Serial3.read();
			break;
	}
	crc+=value;
	uint8_t data;
	switch(serialID){
		case(0):
			data = Serial.read();
			break;
		case(1):
			data = Serial1.read();
			break;
		case(2):
			data = Serial2.read();
			break;
		case(3):
			data = Serial3.read();
			break;
	}

	if(valid)
		*valid = ((crc&0x7F)==data);
	else
		switch(serialID){
		case(0):
			Serial.read();
			break;
		case(1):
			Serial1.read();
			break;
		case(2):
			Serial2.read();
			break;
		case(3):
			Serial3.read();
			break;
	}
		
	return value;
}
