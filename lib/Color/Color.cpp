#include "Color.h"
;

Color::Color() { }

void Color::wait_color(bool color) {
  bool i=1;
  while(i) {
    uint16_t r, g, b, c;
    getRawData_noDelay(&r, &g, &b, &c);
    // colorTemp = tcs.calculateColorTemperature(r, g, b);
    // lux = tcs.calculateLux(r, g, b);
    Serial.print("R: "); Serial.print(r, DEC); Serial.print(" ");
    Serial.print("G: "); Serial.print(g, DEC); Serial.print(" ");
    Serial.print("B: "); Serial.print(b, DEC); Serial.print(" ");
    Serial.print("C: "); Serial.print(c, DEC); Serial.print(" ");

    if (c >= 2000) {
      if (color == 1 && r >= 1.5*b && g >= 1.5*b) {
        Serial.println(" ");
        Serial.println("Yellow detected! Stop 360 degree servo...");
        i=0;
        // break;
      }

      else if (color == 0 && b >= 1.5*r && b >= 1.2*g) {
        Serial.println(" ");
        Serial.println("Blue detected! Stop 360 degree servo...");
        i=0;
        // break;
      }
      Serial.println(" ");
    }

    else {
      Serial.println("   No object to detect!");
    }
  }
}

void Color::getRawData_noDelay(uint16_t *r, uint16_t *g, uint16_t *b, uint16_t *c) {
  *c = tcs.read16(TCS34725_CDATAL);
  *r = tcs.read16(TCS34725_RDATAL);
  *g = tcs.read16(TCS34725_GDATAL);
  *b = tcs.read16(TCS34725_BDATAL);
}
