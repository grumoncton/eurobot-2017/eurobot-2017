#ifndef Detect_Color_h
#define Detect_Color_h

#include <Wire.h>
#include <Arduino.h>
#include "Adafruit_TCS34725.h"

class Color{
public:
  Color();
  Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_700MS, TCS34725_GAIN_1X);
  void wait_color(bool);
  void getRawData_noDelay(uint16_t *, uint16_t *, uint16_t *, uint16_t *);
private:

};
#endif
