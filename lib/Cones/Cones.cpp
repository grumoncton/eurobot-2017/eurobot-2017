#include "Arduino.h"
#include "Cones.h"
#include "Robot.h"
#include "Grabber.h"
#include "Bras.h"
#include "Side.h"
#include "math.h"
#include "TableSide.h"
#include "math.h"

void Cones::getCone(double _x, double _y, int numColours) {

  // Direction to use
  Direction direction = Forwards;

  // Calculate differences
  double diffX = _x - _robot.xPos;
  double diffY = _y - _robot.yPos;

  // Calculate target angle
  double targetA = atan(diffY / diffX);
  if (diffY < 0) targetA = - targetA;

  // How far away are we?
  double dist = sqrt(sq(diffX) + sq(diffY)) - CONE_CENTRE_TO_ROBOT_CENTRE;

  // Go backwards if we're too close
  if (dist < 0) direction = Backwards;

  // Calculate target coords
  double targetX = _robot.xPos + dist * cos(targetA);
  double targetY = _robot.yPos + dist * sin(targetA);

  // Lower and open grabber
  _grabber.prepare();

  // Move to target
  long beforeMillis = millis();
  moveTo(targetX, targetY, direction);
  delay(1000 - min(millis() - beforeMillis, 1000));

  // Close and raise grabber
  _grabber.grab();

  // Determine side based on #colours the cylinder has and the side of the table we're on
  Side side;
  if ((tableSide == Blue && numColours == 1) || (tableSide == Yellow && numColours == 2)) {
    side = Right;
  } else {
    side = Left;
  }

  // Sort cylinder based on side chosen
  _grabber.sort(side);
}

void Cones::getTour(double _x, double _y, int numColours) {
  for (int i = 0; i < 4; i++) {
    getCone(_x, _y, numColours);
    moveRelative(-40, 0, Backwards);
  }
}

void Cones::drop(double x, double y, double a, int numColours) {
  Side side;
  if ((tableSide == Blue && numColours == 1) || (tableSide == Yellow && numColours == 2)) {
    side = Right;
  } else {
    side = Left;
  }

  double targetX, targetY;

  switch (side) {
  case Right:
    targetX = x - DROPPER_CENTRE_TO_ROBOT_CENTRE * sin(a);
    targetY = y + DROPPER_CENTRE_TO_ROBOT_CENTRE * cos(a);
    break;
  case Left:
    targetX = x + DROPPER_CENTRE_TO_ROBOT_CENTRE * sin(a);
    targetY = y - DROPPER_CENTRE_TO_ROBOT_CENTRE * cos(a);
    break;
  }


  moveTo(targetX, targetY, Forwards);
  rotate(a);

  _bras.drop(numColours);
}

