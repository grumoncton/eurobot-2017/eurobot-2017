#ifndef Cones_h
#define Cones_h

#include "TableSide.h"
#include "Robot.h"
#include "Grabber.h"
#include "Bras.h"
#include "Direction.h"
#include "Robot.h"

#define DROPPER_CENTRE_TO_ROBOT_CENTRE OFFSET_RAIL_TO_ROBOT + HALF_RAIL_WIDTH + ROBOT_CENTRE_TO_ROBOT_EDGE

class Cones {
public:
  TableSide tableSide;
  Cones(Robot& robot, Grabber& grabber, Bras& bras): _grabber(grabber), _bras(bras), _robot(robot) { }
  void getCone(double x, double y, int numColours);
  void getTour(double x, double y, int numColours);
  void drop(double x, double y, double a, int numColours); 
  void (* moveTo) (double x, double y, Direction direction);
  void (* moveRelative) (double x, double y, Direction direction);
  void (* rotate) (double a);
private:
  Grabber& _grabber;
  Bras& _bras;
  Robot& _robot;
};

#endif
