#include "Arduino.h"
#include "Pid.h"
;

float Kp, Ki, Kd;

Pid::Pid(float _Kp, float _Ki, float _Kd) {
  Kp = _Kp;
  Ki = _Ki;
  Kd = _Kd;
}
