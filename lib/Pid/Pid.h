#ifndef Pid_h
#define Pid_h

#include "Arduino.h"

class Pid {

public:

  float Kp, Ki, Kd;
  Pid(float Kp, float Ki, float Kd);

};

#endif
