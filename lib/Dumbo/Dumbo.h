#ifndef Dumbo_h
#define Dumbo_h

#include "Arduino.h"
#include "Servo.h"

class Dumbo
{
    public:
      void up();
      void down();
};

#endif
