#include "Arduino.h"
#include "Dumbo.h"
#include "Servo.h"

Servo servo_bas;
Servo servo_haut;

void Dumbo::down() {
  servo_bas.attach(9);
  servo_bas.write(180); // 180 est pour tighter (counterclockwise)  bas seulement
  delay(0);

  servo_haut.attach(10);
  servo_haut.write(180); // 0 est tigher (clockwise) haut seulement
  delay(5500);

  servo_haut.detach();
  pinMode(10,OUTPUT);
  digitalWrite(10,LOW);

  delay(1100);

  servo_bas.detach();
  pinMode(9,OUTPUT);
  digitalWrite(9,LOW);

}

void Dumbo::up() {
  servo_haut.attach(10);
  servo_haut.write(0); // 0 est tigher 180 est pour looser
  delay(1000);

  servo_bas.attach(9);
  servo_bas.write(0);
  delay(3300);

  servo_bas.detach();
  pinMode(9,OUTPUT);
  digitalWrite(9,LOW);

  delay(4700);

  servo_haut.detach();
  pinMode(10,OUTPUT);
  digitalWrite(10,LOW);

}
