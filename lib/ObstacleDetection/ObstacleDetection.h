#ifndef ObstacleDetection_h
#define ObstacleDetection_h

class ObstacleDetection {
public:
  ObstacleDetection(HardwareSerial& serial);
  bool shouldStop();
private:
  HardwareSerial& _serial;
};

#endif
