#include "Arduino.h"
#include "ObstacleDetection.h"
;

ObstacleDetection::ObstacleDetection(HardwareSerial& serial): _serial(serial) { }

bool ObstacleDetection::shouldStop() {
  if (_serial.available() == 0) return false;

  return _serial.read() == 's';
}

